﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OpenHardwareMonitor;
using OpenHardwareMonitor.Hardware;
using System.Linq;
using OpenHardwareMonitor.Collections;
namespace SystemMonitorCore
{
    public class Computer
    {
        private static OpenHardwareMonitor.Hardware.Computer _comp { get; set; } // Static OpenHardwareMonitor.Hardware.Computer object. Encapsulated
        private static Computer _Computer { get; set; } //Static SystemMonitorCore.Computer object. Encapsulated
        private Computer() //Creates OpenHardwareMonitor.Hardware.Computer object for only once. Also it prevents creating of multiple these objects.
        {
            if (_comp == null) // Check whether the first state of the _comp. 
                _comp = new OpenHardwareMonitor.Hardware.Computer
                {
                    CPUEnabled = true,
                    FanControllerEnabled = true,
                    GPUEnabled = true,
                    HDDEnabled = true,
                    MainboardEnabled = true,
                    RAMEnabled = true
                }; //Enables all monitoring features of the Computer.
        }

        /*Creates and opens SystemMonitorCore.Computer object for only once. Also it prevents creating of multiple these objects.*/
        public static Computer CreateAndOpenComputer()
        {           
            if (_Computer == null)
                _Computer = new Computer();
            _comp.Open();
            return _Computer;
        }
        /*Only creates SystemMonitorCore.Computer object for only once. Also it prevents creating of multiple these objects.*/
        public static Computer CreateComputer()
        {
            if (_Computer == null)
                _Computer = new Computer();
            return _Computer;
        }
        /*Overriden ToString Method to see system report.*/
        public override string ToString()
        {
            return _comp.GetReport();
        }
               
        /*It returns specified types of hardwares if and only if this.Hardware obj was created.*/
        public List<IHardware> GetSpecifiedTypesHardwares(HardwareType ht)
        {        
            return _comp.Hardware.Where(x => x.HardwareType.Equals(ht)).ToList();
        }
        /*It returns specified hardware with respect to its identifier if and only if this.Hardware obj was created.*/
        public IHardware GetSpecifiedHardware(Identifier id)
        {
            return _comp.Hardware.Where(x => x.Identifier.Equals(id)).ElementAt(0);
           
        }
        /*It returns all subhardware of specified hardware*/
        public List<IHardware> GetAllSubhardware(IHardware hw)
        {
            return hw != null ? hw.SubHardware.ToList() : null;
        }
        /*It gives full report of specified hardware or subhardware*/
        public string GetReportOfSpecifiedHardware(IHardware hw)
        {
            return hw.GetReport();
        }
        /*It returns all hardware of the computer*/
        public List<IHardware> GetAllHardware()
        {
            return _comp.Hardware.ToList();
        }
    }
}
