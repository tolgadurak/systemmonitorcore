﻿using OpenHardwareMonitor.Hardware;
using OpenHardwareMonitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SystemMonitorCore.Monitor;
using System.Security.Permissions;

namespace SystemMonitorCore.Controller
{
    public class HardwareController : IComponent
    {
        #region  /*Each property stores related hardware itself.*/
        public List<IHardware> CPUs
        {
            get { return computer.GetSpecifiedTypesHardwares(HardwareType.CPU); }
        }

        public List<IHardware> GPUs
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.GpuNvidia);
            }
        }

        public List<IHardware> Mainboards
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.Mainboard);
            }
        }

        public List<IHardware> HDDs
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.HDD);
            }
        }

        public List<IHardware> RAMs
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.RAM);
            }
        }

        public List<IHardware> SuperIO
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.SuperIO);
            }
        }

        public List<IHardware> TBalancer
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.TBalancer);
            }
        }

        public List<IHardware> Heatmaster
        {
            get
            {
                return computer.GetSpecifiedTypesHardwares(HardwareType.Heatmaster);
            }
        }
        public List<IHardware> Hardware { get { return computer.GetAllHardware(); } }
        #endregion
        /*Create an instance of Computer to fetch all hardware with its states*/
        Computer computer = Computer.CreateAndOpenComputer();
         MonitorHardware mhw;
       private static HardwareController _instance = new HardwareController();      
        private HardwareController()
        {            
           
        }    
       //Monitor specified hardware.
        public void ObserveSpecifiedSensor(List<IHardware> hw, SensorType s)
        {
            if (mhw == null)
                mhw = new MonitorHardware();

            mhw.Monitor(hw, s);
        }
        // Returns the instance of HardwareController
        public static HardwareController InitHardwareController()
        {
            return HardwareController._instance;
        }
        // Returns System Report
        public string GetSystemReport()
        {
           
            return computer.ToString();
        }
    }
}
