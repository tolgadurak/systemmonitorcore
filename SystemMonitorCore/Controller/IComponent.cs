﻿using OpenHardwareMonitor;
using OpenHardwareMonitor.Hardware;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemMonitorCore.Controller
{
    /*An interface to be implemented to store specific hardware of the computer*/
    interface IComponent
    {
        /*Each property stores related hardware itself.*/
        List<IHardware> CPUs { get; } 
        List<IHardware> GPUs { get; } 
        List<IHardware> Mainboards { get; }
        List<IHardware> HDDs { get; } 
        List<IHardware> RAMs { get; }
        List<IHardware> SuperIO { get; } 
        List<IHardware> TBalancer { get; }
        List<IHardware> Heatmaster { get; }
        List<IHardware> Hardware { get; }
    }
}
