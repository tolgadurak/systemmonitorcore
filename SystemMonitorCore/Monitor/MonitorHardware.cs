﻿using OpenHardwareMonitor.Hardware;
using OpenHardwareMonitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenHardwareMonitor.Collections;

namespace SystemMonitorCore.Monitor
{
    /*The object of the class is not allowed to be created*/
    public class MonitorHardware
    {
        private List<IHardware> _HWs; //Stores list of hardware to be monitored. 
        private SensorType _ST; //Stores sensor type info to be used as search constraint.
        public Thread thread; // Main thread for each monitor process.
        private string Report = "";
        /*Assign objects, create thread, and pass arguments to be monitored.*/
        public MonitorHardware()
        {
            thread = new Thread(MonitorSensor);
        }

        //Trigger to main thread  
        public void Monitor(List<IHardware> hw, SensorType st)
        {
            _HWs = hw;
            _ST = st;
            if (thread.ThreadState == ThreadState.Unstarted)
                thread.Start();
        }
        // Loop for thread.
        public void MonitorSensor()
        {
            do
            {
                while (!Console.KeyAvailable)
                {
                    foreach (IHardware hardware in _HWs)
                    {
                        ReadSensorValues(hardware);
                        foreach (IHardware sub in hardware.SubHardware)
                        {
                            ReadSensorValues(hardware, sub);
                        }
                    }
                    Report += "\nCtrl + C to exit.";
                    Console.Clear();
                    Console.WriteLine(Report);
                    Thread.Sleep(1000);
                    Report = "";
                }
                Console.Clear();
            } while (Console.ReadKey(true).Key != ConsoleKey.X);
        }

        // Read sensor values of specified hardware
        private void ReadSensorValues(IHardware hardware)
        {
            hardware.Update();
            hardware.Sensors.Where(x => x.SensorType.Equals(_ST))
                 .ToList().ForEach(x =>
                 {
                     Report += String.Format("--{0}--\nHardware Name: {1}\nSensor Name: {2}\nSensor Value: {3}\n\n",
                         hardware.HardwareType, hardware.Name, x.Name, x.Value.HasValue ? x.Value.ToString() : "No value");
                 });

        }
        // Read sensor values of specified subhardware
        private void ReadSensorValues(IHardware hardware, IHardware sub)
        {
            sub.Update();
            sub.Sensors.Where(x => x.SensorType.Equals(_ST))
        .ToList().ForEach(x =>
        {

            Report += String.Format("", hardware.Name)
                + String.Format("--{0}--\nHardware Name: {1}\nSubhardware Name: {2}\nSensor Name: {3}\nSensor Value: {4}\n\n"
                , hardware.HardwareType, hardware.Name, sub.Name, x.Name, x.Value);
        });
        }
    }
}
