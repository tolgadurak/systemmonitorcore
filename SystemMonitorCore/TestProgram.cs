﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenHardwareMonitor;
using OpenHardwareMonitor.Hardware;
using SystemMonitorCore.Controller;
using System.Diagnostics;
using System.Threading;
namespace SystemMonitorCore
{
    class TestProgram
    {
        static void Main(string[] args)
        {

            Console.Title = "System Monitor";
            Console.SetWindowSize(Console.WindowWidth, Console.WindowHeight + 20);
            Console.CancelKeyPress += Console_CancelKeyPress;
           HardwareController hwController = HardwareController.InitHardwareController();
          hwController.ObserveSpecifiedSensor(hwController.GPUs, SensorType.Control);
          
            Console.WriteLine(hwController.GetSystemReport());
            
           
            
        }
        static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
